$(document).ready(function() {

    /* Sel·leccionar totes les imatges a la pàgina;
    registrar en la consola l'atribut alt de cada imagen.*/
    $('img').each(function () {
        console.log($(this).attr('alt'));
    });

    // Sel·leccionar l'element input, després anar al formulari i afegeix-li una classe.
    console.log($('#search :input').addClass('formulari'));


    // Sel·leccionar el ítem que té la classe current dins de la llista #myList 
    //i el·liminar aquesta classe a l'element; després afegir la classe current al següent ítem de la llista.
    $("#myList .current").removeClass("current").next("li").addClass("current");


    //Sel·leccionar l'element select dins de #specials; després anar cap al botó submit.
    $('#specials select');
    $('#specials input:submit').css("color", "red");

    /*Sel·leccionar el primer ítem de la llista en l'element #slideshow; afegir-li la classe 
    current al mateix i  després afegir la classe disabled als elements germans. */

    $("#slideshow li:eq(0)").addClass('current').siblings('li').addClass("disabled");

    /* Afegir 5 nous ítems al final de la llista desordenada #myList. 
    Ajuda: for (var i = 0; i<5; i++) { ... }*/

    for(var i = 0; i<5; i++){
        $("#myList").append("<li>List item " + (i+1) + "</li>");
    };

    //Remoure els ítems impars de la llista.

    $("#myList li:odd").remove();

    //Afegir un altre element h2 i un altre paràgref al últim div.module.
    $(".module").last().append("<h2>Hola :DDD</h2><p>soc un paragraf</p>")

    //Afegir una altre opció a l' element select; donar-li a l'opció afegida el valor Wednesday.

    $("#specials select").append("<option value='wednesday'>Wednesday</option>");

    // Afegir un nou div.module a la pàgina després del últim; després afegir una còpia 
    // d'una de les imatges existents dins del nou div.
   
    $("<div class='module'><h2>hola</h2></div>").insertAfter("div.module:last");
    $("img[alt='fruit']").clone().prependTo("div.module:last");
});