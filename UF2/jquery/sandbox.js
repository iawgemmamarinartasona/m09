
$(document).ready(function() {
    /* Seleccionar tots els elements div que tenen la classe module. */
    console.log("Aquests son els divs amb la classe module:",  $('div.module') );


    /* Especificar tres seleccions que poden seleccionar el tercer ítem de la llista desordenada #myList. 
        ¿Quin és el millor per utilitzar? ¿Per qué? */

    $('#myListItem'); //Amb la id
    $('li.myListItem'); //Especificant el tipus d'element.
    $('li[id=myListItem]'); //Amb l'atribut  

    //La segona opció es la millor, perque es la mes rapida, i guanya temps.

    /*Seleccionar l' element label de l'element input utilizant un selector d'atribut.*/
    $('input[name=q]');

    /* Averiguar quants elements a la pàgina estan ocults (ajuda: .length) */
    $('div:invisible').length

    /* Averiguar quantes imatges a la pàgina poseeixen l' atribut alt. */

    $('#img:alt');
    $('img[alt=*]')

    /* Seleccionar totes les files impars del cos de la taula. */

    $('table:odd');
});
